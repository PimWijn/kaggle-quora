import theano
import numpy as np
import sys
import pickle
import time

from neupy import algorithms, layers, environment


from explore import load_data

environment.reproducible(56)
theano.config.floatX = 'float32'


def stacked_autoencoder_adadelta_classification(x_train, y_train, x_test):
    if len(x_train) != len(y_train) or x_train.shape[1] != x_test.shape[1]:
        sys.exit("Inconsequent dimensions on train/test data!")
    # Use an autoencoder to reduce dimensionality further
    encoder = layers.join(
        # layers for encoding part of autoencoder
        layers.Input(x_train.shape[1]),
        layers.Relu(512),
        layers.Relu(256),
        layers.Relu(128),
    )

    decoder = layers.join(
        # inverse layers for decoding part of autoencoder
        layers.Relu(256),
        layers.Relu(512),
        layers.Relu(x_train.shape[1]),
    )

    autoencoder = algorithms.Momentum(
        connection=encoder > decoder,
        verbose=True,
        step=0.1,
        momentum=0.99,
        shuffle_data=True,
        nesterov=True,
        batch_size=64,
        error='rmse',
    )
    autoencoder.architecture()
    autoencoder.train(x_train, x_train, x_test, x_test, epochs=1)

    x_train_encoded = encoder.output(x_train).eval()
    x_test_encoded = encoder.output(x_test).eval()

    # Use reduced dimensionality data to classify data
    encoded_classifier_layers = layers.join(
        # classifier layers
        layers.PRelu(256),
        layers.Dropout(0.25),
        layers.Softmax(2),
    )

    encoded_classifier = algorithms.Adadelta(
        layers.Input(encoder.output_shape) > encoded_classifier_layers,
        verbose=True,
        step=0.1,
        shuffle_data=True,
        batch_size=64,
        error='binary_crossentropy',
    )
    encoded_classifier.architecture()
    encoded_classifier.train(x_train_encoded, y_train, epochs=10)

    classifier = algorithms.MinibatchGradientDescent(
        encoder > encoded_classifier_layers,
        verbose=True,
        step=0.01,
        shuffle_data=True,
        batch_size=64,
        error='binary_crossentropy',
    )
    classifier.architecture()
    classifier.train(x_train, y_train, epochs=10)

    predictions = classifier.predict(x_test)[:, 0]

    return classifier, predictions


features = np.loadtxt("./Features/train_all_features.txt")

x_tr = features[:, :404]
x_res = features[:, 404]

y_tr = np.zeros((len(features), 2))
y_tr[:, 0] = features[:, 404]
y_tr[:, 1] = 1 - features[:, 404]
print(y_tr)

print('{0} == {1}'.format(np.count_nonzero(x_res), np.count_nonzero(y_tr[:, 0])))
print('{0} == ({1} - {2}) == {3}'.format(np.count_nonzero(y_tr[:, 1]), len(features), np.count_nonzero(y_tr[:, 0]), len(features) - np.count_nonzero(y_tr[:, 0])))

x_te = features[:200, :404]

classifier, pred = stacked_autoencoder_adadelta_classification(x_tr, y_tr, x_te)

# Write classifier to file for further use
f = open('classifier-{0}.bin'.format(round(time.time())), 'wb')
pickle.dump(classifier, f, protocol=2)
f.close()

print(pred)
for idx, prediction in enumerate(pred):
    print('Prediction: {0}, Actual: {1}'.format(prediction, y_tr[idx, 0]))


