import sklearn
import nltk
import numpy

from sklearn.manifold import TSNE
from sklearn.decomposition import TruncatedSVD
from sklearn.metrics.pairwise import pairwise_distances
from sklearn.feature_extraction.text import TfidfVectorizer

from measures import jaccard


import matplotlib.pyplot as plt
import csv


tokenized_sentences = []
dictionary = {}


def load_data(filename="./Data/train_strings.csv"):
    sentences = []
    count = 0


    print("Loading...")
    with open(filename) as csvfile:
        reader = csv.reader(csvfile, delimiter=",")
        for idx, sentence in reader:
            if count >= 500000:
                break
            sentences.append(sentence)
            # tokenized_sentence = nltk.word_tokenize(sentence)
            # tokenized = [token.lower() for token in tokenized_sentence if token not in (',', ':', '?', '.')]
            #
            # vocab = vocab.union(tokenized)
            #
            # tokenized_sentences.append(numpy.array(tokenized))
            count += 1

    tfidf = TfidfVectorizer(stop_words="english")

    print("TfIdf...")
    sparse_vectors = tfidf.fit_transform(sentences)
    print("PCA...")
    tsvd = TruncatedSVD(n_components=300)
    vectors = tsvd.fit_transform(sparse_vectors)
    print(vectors)
    return vectors, sentences

    # for idx, word in enumerate(vocab):
    #     dictionary[word] = idx
    #
    # for sentence in tokenized_sentences:
    #     vectors.append(set(map(lambda x: dictionary[x], sentence)))
    # v = DictVectorizer(sparse=False)
    # global vectors
    # v.fit(dictionary)
    #
    # for sent in tokenized_sentences:
    #     sent = v.transform(sent)


def plot_tsne():
    print("Distance Matrix...")
    matrix = pairwise_distances(vectors[:2000], metric="cosine")
    print("T-SNE...")
    tsne_model = TSNE(n_components=2, metric='precomputed')
    points = tsne_model.fit_transform(matrix)

    fig = plt.figure()
    ax = fig.add_subplot(111)

    def onclick(event):
        print(event.ind)
        for idx in event.ind:
            print(sentences[idx])

    cid = fig.canvas.mpl_connect('pick_event', onclick)

    plt.scatter(points[:, 0], points[:, 1], picker=True)
    plt.show()

if __name__ == '__main__':
    vectors, sentences = load_data()
    plot_tsne()