import csv
import gensim

model = None
with open("./Data/train_strings.csv") as csvfile:
	reader = csv.reader(csvfile, delimiter=",")
	sentences = list(reader)
	s = [b.split() for a,b in sentences]
	model = gensim.models.Word2Vec(s, workers=2)


print(model["woman"])
print(model["woman", "is", "stupid"])

# w2v = model[s1.split()] - model[s2.split()]

print(model.most_similar(positive=["woman", "king"], negative=["man"], topn=1))