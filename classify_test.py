import theano
import numpy as np
import sys
import pickle
import time

from neupy import algorithms, layers, environment

environment.reproducible(56)
theano.config.floatX = 'float32'

files = [
'xaa',
'xab',
'xac',
'xad',
'xae',
'xaf',
'xag',
'xah',
'xai',
'xaj',
'xak',
'xal',
'xam',
'xan',
'xao',
'xap',
'xaq',
'xar',
'xas',
'xat',
'xau',
'xav',
'xaw',
'xax',]


f = open('classifier-1497828990.bin', 'rb')
classifier = pickle.load(f)
f.close()

f = open('submission1.csv', 'w')
f.write('test_id,is_duplicate\n')

for file in files:
    print('Classifying file: ' + file)
    vectors = np.loadtxt("/media/pim/data/" + file)

    predictions = classifier.predict(vectors)[:, 0]

    for idx, pred in enumerate(predictions):
        f.write('{0},{1}\n'.format(idx, pred))

f.close()
