def jaccard(set1, set2):
    return 1 - (len(set1 & set2) / len(set1 | set2))