import csv
from scipy import spatial
import gensim
import string
from nltk.tokenize import word_tokenize
import pickle
import array
import numpy as np
from sklearn.decomposition import TruncatedSVD
import re

from sklearn.feature_extraction.text import TfidfVectorizer

regex = re.compile('[%s]' % re.escape(string.punctuation))

questions = []
corpus =[]

with open("./Data/train.csv") as csvfile:
    reader = csv.reader(csvfile, delimiter=",")

    # Skip header
    next(reader)

    for row in reader:
        questions.append((row[3], row[4], row[5]))
        corpus.append(row[3])
        corpus.append(row[4])

model = gensim.models.Doc2Vec.load("./Data/enwiki_dbow/doc2vec.bin")

print('TfIdf...')
tfidf = TfidfVectorizer(stop_words="english")
tf_idf_vectors = tfidf.fit_transform(corpus)
print('PCA on TfIdf vectors..')
tsvd = TruncatedSVD(n_components=100)
vectors = tsvd.fit_transform(tf_idf_vectors)


results = np.zeros((len(questions), 405))
counter = 0
length = len(questions)
for q in questions:
    if counter % 10000 == 0:
        print((str(counter) + "/" + str(length)))

    clean_1 = word_tokenize(regex.sub('', q[0]))
    clean_2 = word_tokenize(regex.sub('', q[1]))

    v1 = model.infer_vector(clean_1)
    v2 = model.infer_vector(clean_2)

    v1_idf = vectors[counter * 2]      # index of q1 in vectors
    v2_idf = vectors[counter * 2 + 1]  # index of q2 in vectors

    euclidian_distance = spatial.distance.euclidean(v1, v2)
    cosine_distance = spatial.distance.cosine(v1, v2)

    tfidf_euclidean = spatial.distance.euclidean(v1_idf, v2_idf)
    tfidf_cosine = spatial.distance.cosine(v1_idf, v2_idf)

    results[counter, :300] = v1 - v2
    results[counter, 300:400] = v1_idf - v2_idf
    results[counter, 400] = euclidian_distance
    results[counter, 401] = cosine_distance
    results[counter, 402] = tfidf_euclidean
    results[counter, 403] = tfidf_cosine
    results[counter, 404] = q[2]
    counter = counter + 1
print((str(counter) + "/" + str(length)))

np.savetxt("./Features/train_all_features.txt", results)
