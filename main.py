import csv

questions = set()
same_question = set()

with open("./Data/train.csv") as csvfile:
    reader = csv.reader(csvfile, delimiter=",")

    # Skip header
    reader.next() 

    for row in reader:
        questions.add((int(row[1]), row[3]))
        questions.add((int(row[2]), row[4]))
        if(int(row[5]) == 1):
            same_question.add((int(row[1]), int(row[2])))

question_list = list(questions)
question_list = sorted(question_list, key=lambda x: x[0])

same_question = sorted(same_question, key=lambda x: x[0])

with open("./Data/train_strings.csv", "wb") as newfile:
    writer = csv.writer(newfile)
    writer.writerows(question_list)

with open("./Data/train_same.csv", "wb") as newfile:
    writer = csv.writer(newfile)
    writer.writerows(same_question)